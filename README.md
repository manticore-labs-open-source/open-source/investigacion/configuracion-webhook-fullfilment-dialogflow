# Configuracion webhook fullfilment dialogflow

Aquí se muestra como configurar un webhook fullfilment dialogflow.


# Tutorial 

* El webhook debera estar en una URL publica puedes consultar los tutoriales de creacion de webhook y creacion de proyecto en c9.

* Tener un agente al que se desea agregar el webhook. 

* Se debe activar el webhook como se muestra en la imagen **ENABLED**

* Finalmente se debe ubicar en la URL la URL de el proyecto c9. Dialogflow espera que esta sea https. 



   ![](imagenes/dialog.png)





<a href="https://twitter.com/alimonse29" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @alimonse29 </a><br>

